#!/bin/bash
TORSTARTUPFILE="$HOME/tor-browser_en-US/Browser/start-tor-browser"
URL=$1
# Find the most recently used Tor Browser window (from https://unix.stackexchange.com/questions/439037/how-to-activate-most-recently-focused-app-window-in-xfce4-xfwm4 )

# Changes
# 2022-02-11 - pidfile support

if pgrep -F "$HOME/.torsh.pid"; then
    zenity --info --text "Already running under pid $(cat \"$HOME/.torsh.pid\")"
    exit 1
fi

echo $$ > "$HOME/.torsh.pid"

TORID=$( (for WINID in $(wmctrl -l | grep "Tor Browser" | awk '{print $1}'); do
    echo $(xprop -id $WINID | grep _NET_WM_USER_TIME\(CARDINAL\) | cut -d"=" -f2) $WINID
done) | sort -n | tail -n1 | awk '{print $2}' )

echo "TORID: $TORID"

CLIPBOARD="clipboard"

function doquit
{
    rm "$HOME/.torsh.pid"
    exit $1
}

function sendkeys
{
    sleep 0.2
    if [ -n "$URL" ]; then
        # Tor window ID is valid long before we see the window,
        # so try to activate it until we succeed
        FOCUSFAIL=$(xdotool windowactivate $TORID 2>&1)
        NEWTAB=1
        COUNT=0
        while [ -n "$FOCUSFAIL" ]; do
            if [ $COUNT -gt 100 ]; then echo "Timed out."; doquit 1; fi
            FOCUSFAIL=$(xdotool windowactivate $TORID 2>&1)
            echo "Waiting $FOCUSFAIL"
            sleep 0.2
            NEWTAB=0 # Seems like we started a new window, so don't open a new tab
            echo "NEWTAB=0"
            COUNT=$(( $COUNT +1 ))
        done
        # Back up clipboard
        CLIP=$(xclip -selection $CLIPBOARD -o 2>/dev/null)

        COUNT=0
        echo -ne "$URL" | xclip -i -selection $CLIPBOARD
        # Sometimes this doesn't work, so do until it does
        while ! [ "$URL" == $(xclip -selection $CLIPBOARD -o 2>/dev/null) ] && [ $COUNT -lt 100 ]; do
            echo "xclip copy failed, trying again"
            echo -ne "$URL" | xclip -i -selection $CLIPBOARD
            COUT=$(( $COUNT +1 ))
            sleep 0.2
        done

        if ! [ "$URL" == $(xclip -selection $CLIPBOARD -o 2>/dev/null) ]; then
            # The loop above failed, bail out
            doquit 1
        fi

        # Note: Play with the following code block...
        # the sleeps are tricky here, depends on number of monitors, if Tor is
        # playing something in fullscreen, etc.
        echo "Pasting url $URL into address bar"
#        xdotool windowfocus $TORID
        xdotool windowactivate $TORID
        # Use CTRL-T and try to fling the URL into the address bar
        if [ $NEWTAB -eq 1 ]; then
            xdotool key --window $TORID ctrl+t
        fi
        sleep 0.2
        xdotool windowactivate $TORID
        xdotool key --window $TORID ctrl+l
        sleep 0.2
#        echo "$URL" | xvkbd -file -
        xdotool key --window $TORID ctrl+v
#        xdotool type --window $TORID "$URL"
        xdotool key --window $TORID Return
        # Restore clipboard
        echo -ne "$CLIP" | xclip -i -selection $CLIPBOARD

    else
        xdotool key --window $TORID ctrl+n
    fi
}

if [ -n "$TORID" ]; then
    # Tor is running
    echo "Tor Browser already running in window $TORID"
#    zenity --error --text "$TORID"
    sendkeys
else
    # Start browser
    echo "No instance found, launching Tor Browser"
    if ! [ -e "$TORSTARTUPFILE" ]; then
        zenity --error --text "Cannot find Tor Browser at $TORSTARTUPFILE."
        doquit 1
    fi
    # Launch browser and then wait for window to appear
    $TORSTARTUPFILE &

    # If no URL provided, exit here, we don't want any new windows
    if [ -z "$URL" ]; then
        doquit 0
    fi

    COUNT=0
    while [ -z $TORID ]; do
        if [ $COUNT -gt 100 ]; then echo "Timed out."; doquit 1; fi
        TORID=$(xdotool search --name "Tor Browser" | head -n1)
        echo "TORID=$TORID"
        sleep 0.5
        COUNT=$(( $COUNT +1 ))
    done
    # Found it
    echo "Tor Browser now running in window $TORID"
    sendkeys
fi

doquit 0
