# tor-open-url

A hack to open a URL in Tor Browser (Linux desktop environment)

There is no easy way to open an URL in Tor Browser (say by clicking on a link, etc.). See [here](https://askubuntu.com/questions/542372/set-tor-browser-as-default-browser), [here](https://trac.torproject.org/projects/tor/ticket/15185), [here](https://tor.stackexchange.com/questions/84/how-do-i-create-a-launcher-script-that-opens-an-url-in-torbrowser-from-the-bu#1109), [here](https://github.com/micahflee/torbrowser-launcher/issues/157), [here](https://github.com/micahflee/torbrowser-launcher/issues/153), [here](https://www.reddit.com/r/TOR/comments/7xqibo/how_to_open_a_new_link_in_a_running_instance_of/), . This script tries to remedy this by copying the URL to the clipboard, and then interacting with the Tor Browser window using `xdotool` (open a new tab, paste URL, press Enter).

If Tor Browser is not currently running, the script will start a new instance and attempt to wait until the window is ready before pasting the URL.

## Security warning

This script leaks URLs to the system, so use with caution.

- Any URLs you open with this script (e.g., by clicking from outside of Tor Browser) will be copied to the clipboard, so keep this in mind. The script attempts to back up whatever was in the clipboard before the link was clicked, and then restore it.
- URLs opened with this script will also be visible in the process list until the script finishes.
- The script naively looks for a window with the name "Tor Browser" and interacts with it using `xdotool`.

## Requirements

- xclip `apt install xclip`
- xdotool `apt install xdotool`
- bash
- zenity (optional)
- Tor Browser unpacked to `~/tor-browser_en-US/` (adjust `TORSTARTUPFILE` in `tor-open-url.sh` if your setup is different)

## Install

- Add contents of `mimeapps.list` to `~/.local/share/applications/mimeapps.list`
- Copy `tor.desktop` to `~/.local/share/applications/`
- Copy `tor-open-url.sh` to `~/bin` (for example), and make sure `~/bin` is in your PATH
- Set "Tor Browser" to be your default browser (usually something like Settings > Default Applications in your window manager/desktop environment)
- Click URLs!

## Bugs

- Sometimes the URL isn't pasted correctly, usually when the script times out waiting for Tor Browser to start, so you have to click it again.
- Tested on Arch, Debian, Ubuntu